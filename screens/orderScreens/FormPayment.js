import React, { Component } from 'react'
import StripeCheckout from 'expo-stripe-checkout';

class Paiment extends Component {
  static navigationOptions = {
    title: 'Payment',
  }
  onPaymentSuccess = (token) => {
    this.setState({ token })
  }
  onClose = () => {
    //TODO: redirect to confirmation screen
  }
  render() {
    return <StripeCheckout
      publicKey="pk_test_YpYtfVqOqBFqN09InxI1ksEY"
      amount={100000}
      imageUrl="https://pbs.twimg.com/profile_images/778378996580888577/MFKh-pNn_400x400.jpg"
      storeName="Rocketeer Checkout"
      description="Buy your rocket"
      currency="USD"
      allowRememberMe={false}
      prepopulatedEmail="test@test.com"
      onClose={this.onClose}
      onPaymentSuccess={this.onPaymentSuccess}
    />
  }
}

export default Paiment;

