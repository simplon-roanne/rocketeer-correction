import React, {Component} from 'react'
import {Text, View, TouchableOpacity} from 'react-native';
import styles from '../styles/Styles';
import {getTotal} from "../../service/Cart";
import {order} from "../../service/WooCommerce";

class Paiment extends Component {
    static navigationOptions = {
        title: 'Confirmation',
    };

    state = {
        total: 0,
        success: false
    };

    async componentDidMount() {
        this.setState({
            total: await getTotal()
        })
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <View style={styles.mainContainerBlue}>
                    <View style={styles.containerFormShipping}>
                        {this.state.success ? (
                            <Text style={[styles.text, {color: 'white'}]}>Order sent. Thank you !</Text>
                        ) : (
                            <View>
                                <Text style={[styles.text, {color: 'white'}]}>Total Amount : {this.state.total} M
                                    €</Text>
                                <Text style={[styles.text, {color: 'white'}]}>Payment at the delivery</Text>
                                <TouchableOpacity
                                    style={styles.buttonType2}
                                    onPress={() => this.sendOrder()}>
                                    <Text style={styles.textButtonType2}>Order</Text>
                                </TouchableOpacity>
                            </View>
                        )}
                    </View>
                </View>
            </View>
        );
    }

    async sendOrder() {
        await order();
        this.setState({success: true})
    }
}

export default Paiment;

