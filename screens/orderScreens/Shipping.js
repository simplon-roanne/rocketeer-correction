import React, {Component} from 'react'
import {Text, TouchableOpacity, View} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import styles from '../styles/Styles';
import {getShipping, setShipping} from "../../service/Shipping";


class Shipping extends Component {
    static navigationOptions = {
        title: 'Shipping',
    };

    state = {
        shipping : {
            firstname : '',
            lastname : '',
            launchSite : '',
            country : '',
        }
    };

    render() {
        return (
            <View style={styles.mainContainer}>
                <View style={styles.mainContainerBlue}>

                    <View style={styles.containerFormShipping}>
                        <Text style={styles.labelInput}>First Name</Text>
                        <TextInput
                            style={styles.textInput}
                            onChangeText={(firstname) => this.setState({shipping : {...this.state.shipping, firstname}})}
                        />
                        <Text style={styles.labelInput}>Last Name</Text>
                        <TextInput
                            style={styles.textInput}
                            onChangeText={(lastname) => this.setState({shipping : {...this.state.shipping, lastname}})}
                        />
                        <Text style={styles.labelInput}>Launch Site</Text>
                        <TextInput
                            style={styles.textInput}
                            onChangeText={(launchSite) => this.setState({shipping : {...this.state.shipping, launchSite}})}
                        />
                        <Text style={styles.labelInput}>Country</Text>
                        <TextInput
                            style={styles.textInput}
                            onChangeText={(country) => this.setState({shipping : {...this.state.shipping, country}})}
                        />
                        <TouchableOpacity
                            style={styles.buttonType2}
                            onPress={() => this.saveShipping()}>
                            <Text style={styles.textButtonType2}>Confirmation</Text>
                        </TouchableOpacity>
                    </View>

                </View>

            </View>
        );
    }

    async saveShipping() {
        await setShipping(this.state.shipping);
        this.props.navigation.navigate('OrderThirdStep')
    }
}

export default Shipping;

