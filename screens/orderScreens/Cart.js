import React, {Component} from 'react'
import {Image, Text, TouchableOpacity, View} from 'react-native'
import styles from '../styles/Styles'

import HeaderDrawer from '../../components/HeaderDrawer'
import Colors from '../../constants/Colors'
import {getCart, pushToCart, removeFromCart} from "../../service/Cart";
import {createAppContainer, createStackNavigator} from "react-navigation";
import Shipping from '../orderScreens/Shipping'
import Paiment from './Paiment';

//import FormPayment from './FormPayment';


class Cart extends Component {
    state = {
        cart: []
    };

    static navigationOptions = {
        title: 'Cart',
        header: null
    };

    async componentDidMount() {
        const productSelected = this.props.navigation.getParam('productSelected');
        if (productSelected) {
            await pushToCart(productSelected);
        }
        this.setState({
            cart: await getCart()
        });
    }

    async removeFromCart(product) {
        await removeFromCart(product);
        this.setState({
            cart: await getCart()
        });
    }

    _cartProductsView() {
        return this.state.cart.map((product, i) => (
            <View style={[styles.containerProductsList, {backgroundColor: "white", margin: 20}]} key={i}>
                <View style={styles.containerEachProduct}>
                    <View style={styles.containerProductPicture}>
                        <Image style={[styles.imageProductList, {width: 150, height: 120}]}
                               source={{uri: product.images[0].src}}/>
                    </View>

                    <View style={[styles.containerProductText, {
                        backgroundColor: Colors.smoothGrey,
                        marginRight: 0
                    }]}>
                        <Text style={styles.titleProductList}>{product.name}</Text>
                        <Text style={[styles.text, {textAlign: 'justify'}]}> Price : {product.price}€</Text>
                        <Text style={[styles.text, {textAlign: 'justify'}]}> Quantity : 1</Text>
                        <TouchableOpacity
                            style={[styles.buttonType1, {alignSelf: 'center'}]}
                            onPress={() => this.removeFromCart(product)}
                        >
                            <Text style={[styles.textButtonType1, {
                                fontSize: 12,
                                marginTop: 10,
                                marginBottom: 10
                            }]}> Remove</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        ));
    }

    render() {
        return (
            <View style={styles.mainContainer}>

                <View style={{backgroundColor: Colors.brandColor, margin: 20, flex: 1}}>

                    {this._cartProductsView()}

                    <TouchableOpacity
                        style={[styles.buttonType2, {alignSelf: 'center'}]}
                        onPress={() => this.props.navigation.navigate('OrderSecondStep')}
                    >
                        <Text style={[styles.textButtonType2, {
                            fontSize: 12,
                            marginTop: 10,
                            marginBottom: 10
                        }]}> Shipping</Text>
                    </TouchableOpacity>


                </View>
            </View>
        )
    }
}

const AppNavigator = createStackNavigator({
        OrderFisrtStep: {
            screen: Cart
        },
        OrderSecondStep: {
            screen: Shipping
        },
        OrderThirdStep: {
            screen: Paiment
        },
        // OrderFourthStep: {
        //   screen: FormPayment
        // }
    }, {
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: Colors.brandColor,

            },
            headerTintColor: '#fff',
        }
    }
);

export default createAppContainer(AppNavigator);