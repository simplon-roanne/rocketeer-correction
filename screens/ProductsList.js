import React, {Component} from 'react'
import {AsyncStorage, Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {createAppContainer, createStackNavigator} from "react-navigation";
import styles from './styles/Styles'

import ProductSheet from "./ProductSheet";
import HeaderDrawer from '../components/HeaderDrawer';
import Colors from '../constants/Colors'
import Cart from "./orderScreens/Cart";


class ProductList extends Component {

  state = {
    products: []
  };

  static navigationOptions = {
    header: null,
  };

    //Here, we get all data from the api that is storage in LocalStorage.
    //Then we choose only data about the first product in the state
    async componentWillMount() {
        let values = await AsyncStorage.getItem("products");
        let products = await JSON.parse(values);
        this.setState({
            products: products,
        });
    }

    //function that take a string and cut it properly
    static cutString(s, n) {
        var cut = s.indexOf(' ', n);
        if (cut == -1) return s;
        return s.substring(0, cut)
    }


    render() {
        return (
            <ScrollView>
                <View style={styles.mainContainer}>
                    <HeaderDrawer navigation={this.props.navigation} routeName={'Our Rockets'}></HeaderDrawer>

                    <View style={styles.containerProductsList}>

                        {this.state.products.map((product, i) => {

                            //we get the description for each product and reduce it to 170 caracters
                            var p = product.description;
                            var regex = /<[^>]*>/g;
                            let description = p.replace(regex, '');
                            let shortDescription = ProductList.cutString(description, 170);


                            return (

                                <View style={styles.containerEachProduct} key={i}>
                                    <View style={styles.containerProductPicture}>
                                        <Image
                                            style={{width: '100%', height: "100%"}}
                                            resizeMode="center"
                                            source={{uri: product.images[0].src}}></Image>
                                    </View>

                                    <View style={styles.containerProductText}>
                                        <Text style={styles.titleProductList}>{product.name}</Text>
                                        <Text style={styles.text}>{shortDescription}...</Text>


                                        <TouchableOpacity
                                            style={[styles.buttonType1, {alignSelf: 'center'}]}
                                            onPress={() => this.props.navigation.navigate('ProductSheet', {productSelected: product})}>
                                            <Text style={[styles.textButtonType1, {
                                                fontSize: 12,
                                                marginTop: 10,
                                                marginBottom: 10
                                            }]}>View more</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )
                        })}
                    </View>

                </View>
            </ScrollView>
        );
    }
}

const AppNavigator = createStackNavigator({
        "Product List": {
            screen: ProductList
        },
        ProductSheet: {
            screen: ProductSheet
        },
        Cart: {
            screen: Cart
        },
    }, {
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: Colors.brandColor,

            },
            headerTintColor: '#fff',
        }
    }
);

export default createAppContainer(AppNavigator);

