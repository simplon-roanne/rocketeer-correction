# Rocketeer

## Getting ready

First, clone repository :
```cli
git clone https://github.com/Mickael42/Rocketeer.git rocketeer
```

Then move into the directory :
```cli
cd rocketeer
```

and
```cli
npm install
```

## Run app in local with Expo

You will need install expo-cli globaly :
```cli
npm install -g expo-cli
```

Run the following command
```cli
expo start
```

And flash QR with Expo app (You will need install the app on your phone)