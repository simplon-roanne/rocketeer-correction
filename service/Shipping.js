import {AsyncStorage} from "react-native";

export const setShipping  = async (shipping) => {
    await AsyncStorage.setItem('shipping', JSON.stringify(shipping));
};

export const getShipping  = async () => {
    return JSON.parse(await AsyncStorage.getItem('shipping'));
};