import {AsyncStorage} from 'react-native';

const setCart = async function (newCart) {
    await AsyncStorage.setItem('cart', JSON.stringify(newCart));
};

export const getCart = async function () {
    let json = await AsyncStorage.getItem("cart");
    let cart = await JSON.parse(json);
    return cart.filter(v => v) || [];
};

export const pushToCart = async function (product) {
    let cart = await getCart();
    if (cart.find(currentProduct => currentProduct.id === product.id)) {
        return;
    }
    cart.push(product);
    await setCart(cart);
};

export const removeFromCart = async function (product) {
    let cart = await getCart();
    cart = cart.filter(currentProduct => currentProduct.id !== product.id);
    await setCart(cart);
};

export const getTotal = async function () {
    const products = await getCart();
    let total = 0;
    products.forEach(product => total += product.price);
    return total
};