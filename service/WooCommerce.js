import WooCommerceAPI from 'react-native-woocommerce-api';
import {getShipping} from "./Shipping";
import {getCart} from "./Cart";

const api = new WooCommerceAPI({
    url: 'https://rocketeer.simplon-roanne.com', // Your store URL
    ssl: true,
    consumerKey: 'ck_3c914d2597381f3e7ba8e4bdb33d5c25b4a6b97c', // Your consumer secret
    consumerSecret: 'cs_d346f5cf4b26af1ef6ce28e69c9734efea2842a8', // Your consumer secret
    wpAPI: true, // Enable the WP REST API integration
    version: 'wc/v3', // WooCommerce WP REST API version
    queryStringAuth: true
});


export const getProducts = async function () {
    try {
        return await api.get('products', {});
    } catch (e) {
        console.log("Error", e);
    }
};

export const order = async function () {
    const shipping = await getShipping();
    const cart = await getCart();

    const response = await api.post('orders', {
        payment_method: "bacs",
        payment_method_title: "Direct Bank Transfer",
        set_paid: false,
        billing: {
            first_name: shipping.firstname,
            last_name: shipping.lastname,
            address_1: "27 Rue Lucien Langénieux",
            address_2: "",
            city: "Roanne",
            postcode: "42300",
            country: shipping.country,
            email: "john.doe@example.com",
        },
        shipping: {
            first_name: shipping.firstname,
            last_name: shipping.lastname,
            address_1: "27 Rue Lucien Langénieux",
            address_2: "",
            city: "Roanne",
            postcode: "42300",
            country: shipping.country,
        },
        line_items: cart.map(product => {
            return {
                product_id: product.id,
                quantity: 1
            }
        }),
        shipping_lines: [
            {
                method_id: "flat_rate",
                method_title: "Flat Rate",
                total: "10000000"
            }
        ]
    });

    return response;
};


