import React from 'react';
import { createStackNavigator } from 'react-navigation';
import Register from '../screens/Register';
import HeaderNav from '../components/HeaderNav';
import Colors from '../constants/Colors';

const AuthStack = createStackNavigator(
  {
    register: {
      screen: Register
    },
  },

  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: Colors.brandColor,
        borderBottomColor : 'white',
        borderBottomWidth: 2
      },
      headerTitle: <HeaderNav></HeaderNav>
    }
  }
);

export default AuthStack;
